/**
 * * #%L
 * *
 * * %%
 * * Copyright (C) 2014-2020 Healthcare Services Platform Consortium
 * * %%
 * * Licensed under the Apache License, Version 2.0 (the "License");
 * * you may not use this file except in compliance with the License.
 * * You may obtain a copy of the License at
 * *
 * *      http://www.apache.org/licenses/LICENSE-2.0
 * *
 * * Unless required by applicable law or agreed to in writing, software
 * * distributed under the License is distributed on an "AS IS" BASIS,
 * * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * * See the License for the specific language governing permissions and
 * * limitations under the License.
 * * #L%
 */

package org.hspconsortium.platform.api.fhir.config;

import org.hspconsortium.platform.api.fhir.DatabaseProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Configuration
@EnableTransactionManagement()
@PropertySource("classpath:/config/mysql.properties")
@EnableConfigurationProperties(DatabaseProperties.class)
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class MySQLConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(MySQLConfig.class);

    private DatabaseProperties databaseProperties;

    @Value("${hspc.platform.api.fhir.hibernate.dialect}")
    private String hibernateDialect;

    @Value("${hibernate.search.default.indexBase}")
    private String luceneBase;

    @Value("${hspc.platform.api.fhir.hibernate.indexSourceUrl:}")
    private String indexSourceUrl;

    @Value("${hspc.platform.api.fhir.allowExternalReferences:true}")
    private boolean allowExternalReferences;

    @Resource(type = DatabaseProperties.class)
    public MySQLConfig setDatabaseProperties(DatabaseProperties databaseProperties) {
        this.databaseProperties = databaseProperties;
        return this;
    }

    protected DatabaseProperties getDatabaseProperties() {
        return databaseProperties;
    }

    /**
     * Configure FHIR properties around the the JPA server via this bean
     */
//    @Bean()
//    public DaoConfig daoConfig() {
//        DaoConfig retVal = new DaoConfig();
//        retVal.setAllowMultipleDelete(true);
//        retVal.setAllowExternalReferences(allowExternalReferences);
//        retVal.setReuseCachedSearchResultsForMillis(null);
//        // Enable below when HAPI has fixed the issue with ":contains"
//        retVal.setAllowContainsSearches(true);
//        return retVal;
//    }
//
//    @Bean
//    public ModelConfig modelConfig() {
//        return daoConfig().getModelConfig();
//    }
    @Bean//(name = {"noSchemaDataSource"})
    public DataSource noSchemaDataSource() {
        // create a datasource that doesn't have a schema in the url
        DataSourceProperties db = getDatabaseProperties().getDataSource();

        String urlNoSchema = null;
        for (String schema : db.getSchema()) {
            if (db.getUrl().toLowerCase().contains(schema.toLowerCase())) {
                urlNoSchema = db.getUrl().toLowerCase().substring(0, db.getUrl().toLowerCase().indexOf(schema.toLowerCase()));
                break;
            }
        }

        if (urlNoSchema == null) {
            throw new RuntimeException("Unable to create noSchemaDataSource for " + db.getUrl());
        }

        DataSourceBuilder factory = DataSourceBuilder
                .create(db.getClassLoader())
//                .driverClassName(db.getDriverClassName())
                .username(db.getUsername())
                .password(db.getPassword())
                .url(urlNoSchema);
        DataSource dataSource = factory.build();

        if (dataSource instanceof org.apache.tomcat.jdbc.pool.DataSource) {
            ((org.apache.tomcat.jdbc.pool.DataSource) dataSource).getPoolProperties().setTestOnBorrow(true);
            ((org.apache.tomcat.jdbc.pool.DataSource) dataSource).getPoolProperties().setValidationQuery("SELECT 1");
        }
        // try it out
        try {
            Connection connection = dataSource.getConnection();
        } catch (SQLException e) {
            LOGGER.error("Error creating noSchemaDataSource", e);
            throw new RuntimeException(e);
        }
        return dataSource;
    }
}
