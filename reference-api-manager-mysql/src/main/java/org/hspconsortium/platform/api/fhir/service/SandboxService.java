/**
 *  * #%L
 *  *
 *  * %%
 *  * Copyright (C) 2014-2020 Healthcare Services Platform Consortium
 *  * %%
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  * #L%
 */

package org.hspconsortium.platform.api.fhir.service;

import org.hspconsortium.platform.api.fhir.model.DataSet;
import org.hspconsortium.platform.api.fhir.model.Sandbox;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

public interface SandboxService {
    void reset();

    Collection<String> allTenantNames();

    Collection<Sandbox> allSandboxes();

    Sandbox save(Sandbox sandbox, DataSet dataSet);

    void clone(Sandbox newSandbox, Sandbox clonedSandbox);

    Sandbox get(String teamId);

    boolean remove(String teamId);

    boolean verifyUser(HttpServletRequest request, String sandboxId);

}
