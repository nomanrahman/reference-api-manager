FROM openjdk:8-jdk-alpine
ADD reference-api-manager-webapp/target/hspc-reference-api-manager-webapp*.jar app.jar
RUN apk add --update mysql mysql-client
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar app.jar" ]