/**
 *  * #%L
 *  *
 *  * %%
 *  * Copyright (C) 2014-2020 Healthcare Services Platform Consortium
 *  * %%
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  * #L%
 */

package org.hspconsortium.platform.api.controller;

import org.apache.commons.lang3.Validate;
import org.hspconsortium.platform.api.fhir.model.DataSet;
import org.hspconsortium.platform.api.fhir.model.Sandbox;
import org.hspconsortium.platform.api.fhir.service.SandboxService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Collection;

@RestController
@RequestMapping("${hspc.platform.api.sandboxPath:/sandbox}")
//@Profile("default")
public class SandboxController {

    @Value("${hspc.platform.api.sandbox.name}")
    private String sandboxName;

    private SandboxService sandboxService;

    @Autowired
    public SandboxController(SandboxService sandboxService) {
        this.sandboxService = sandboxService;
    }

//    @RequestMapping(method = RequestMethod.PUT) // TODO: this save method is not used : Check
//    public Sandbox save(@NotNull @RequestBody Sandbox sandbox, @RequestParam(value = "dataSet", required = false) DataSet dataSet) {
//        Validate.notNull(sandbox);
//        Validate.notNull(sandbox.getTeamId());
//
////        don't validate the name to allow for initialization
////        validate(sandbox.getTeamId());
//        if (dataSet == null) {
//            dataSet = DataSet.NONE;
//        }
//
//        return sandboxService.save(sandbox, dataSet);
//    }

    @RequestMapping(method = RequestMethod.GET)
    public Sandbox get() {
        Sandbox existing = sandboxService.get(sandboxName);
        if (existing == null) {
            throw new ResourceNotFoundException("Sandbox [" + sandboxName + "] is not found");
        }
        return existing;
    }

    @RequestMapping(value = "/allTenants", method = RequestMethod.GET) // TODO: this was originally /sandbox in the ManagementController, but since the endpoint here already has /sandbox, I added /allTenants
    public Collection<String> all() {
        return sandboxService.allTenantNames();
    }

    @RequestMapping(value = "/allSandboxes", method = RequestMethod.GET) // TODO: this was originally not in the ManagementController, but since the endpoint here already has /sandbox, I added /allTenants
    public Collection<Sandbox> allSandboxes() {
        return sandboxService.allSandboxes();
    }

    // We don't reset Sandbox anymore
//    @RequestMapping(path = "/reset", method = RequestMethod.POST)
//    public String reset(@RequestBody ResetSandboxCommand resetSandboxCommand) {
//        sandboxService.reset(sandboxName, resetSandboxCommand.getDataSet());
//        return "Success";
//    }

    @RequestMapping(value = "/{teamId}", method = RequestMethod.DELETE)
    public boolean delete(@PathVariable String teamId) {
        validate(teamId);
        return sandboxService.remove(teamId);
    }

    private void validate(String teamId) {
        try {
            SandboxReservedName.valueOf(teamId);
            // oh no, this is a reserved name
            throw new RuntimeException("Sandbox [" + teamId + "] is not allowed.");
        } catch (IllegalArgumentException e) {
            // good, not a reserved name
        }
    }
}
